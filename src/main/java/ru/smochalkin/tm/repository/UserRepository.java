package ru.smochalkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.exception.entity.UserNotFoundException;
import ru.smochalkin.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @NotNull
    public User findByLogin(@NotNull final String login) {
        return list.stream()
                .filter(e -> e.getLogin().equals(login))
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        @NotNull final User user = findByLogin(login);
        this.remove(user);
    }

    @Override
    public Boolean isLogin(@NotNull final String login) {
        return list.stream().anyMatch(e -> e.getLogin().equals(login));
    }

}