package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-start-by-index";
    }

    @Override
    @NotNull
    public String description() {
        return "Start project by index.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.print("Enter index: ");
        @NotNull Integer index = TerminalUtil.nextInt();
        index--;
        serviceLocator.getProjectService().findByIndex(userId, index);
        serviceLocator.getProjectService().updateStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
